import React from 'react'
import Home from './page/Home'
import About from './page/About'
import Projects from './page/Projects'
import SinglePost from './page/SinglePost'
import SingleProject from './page/SingleProject'
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Blog from './page/Blog'
import NotFound from './page/NotFound'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import './index.css'
import Results from './page/Results'
import { ContentfulClient, ContentfulProvider } from 'react-contentful';

const contentfulClient = new ContentfulClient({
  accessToken: process.env.REACT_APP_CONTENT_DELIVERY_API,
  space: process.env.REACT_APP_SPACE_ID
});

function App() {
  return (
  <ContentfulProvider client={contentfulClient}>
    <Router>
      <Navbar/>
      <Switch>
        <Route path="/" exact component={ Home }/>
        <Route path="/blog" exact component={ Blog } />
        <Route path="/blog/:blogslug" component={ SinglePost } />
        <Route path="/projects" exact component={ Projects } />
        <Route path="/projects/:projectslug" component={ SingleProject } />
        <Route path="/search" component={ Results } />
        <Route path="/about" component={ About } />
        <Route path="/404" component={ NotFound } />
        <Redirect from="*" to="/404"/>
      </Switch>
      <Footer/>
      </Router>
    </ContentfulProvider>
  );
}

export default App;