import React from 'react'
import { Button, IconButton, Typography, AppBar, Toolbar} from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { Cake } from '@material-ui/icons'
import useStyles from '../styles'

const Navbar = () => {
  const classes = useStyles()

  return (
      <AppBar position="relative">
        <Toolbar>
          <NavLink to="/" exact>
            <IconButton aria-controls="simple-menu" aria-haspopup="false" aria-label="logo" className={classes.mainlogo}>
              <Cake />
              <Typography variant="h6">Contentful portfolio</Typography>
            </IconButton>
          </NavLink>
          <NavLink to="/about">
            <Button aria-controls="simple-menu" aria-haspopup="false">
                <Typography variant="h6">About</Typography>
            </Button>
          </NavLink>
          <NavLink to="/projects">
            <Button aria-controls="simple-menu" aria-haspopup="false">
                <Typography variant="h6">Projects</Typography>
            </Button>
          </NavLink>
          <NavLink to="/blog">
            <Button aria-controls="simple-menu" aria-haspopup="false">
                <Typography variant="h6">Blog</Typography>
            </Button>
          </NavLink>
        </Toolbar>
      </AppBar>
  )
}

export default Navbar