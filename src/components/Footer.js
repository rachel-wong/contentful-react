import React from 'react'
import { Button, Typography, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'
import { NavLink } from 'react-router-dom';

const footerClasses = makeStyles((theme) => ({
  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    height: "100%",
    padding: '0 30px',
  },
  nav: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end"
  }
}))

const Footer = () => {

  return (
    <footer className={ footerClasses.root }>
        <Container className={ footerClasses.nav }>
          <NavLink to="/" exact>
            <Button aria-haspopup="false">
                <Typography variant="h6">Home</Typography>
            </Button>
          </NavLink>
          <NavLink to="/about">
            <Button aria-haspopup="false">
                <Typography variant="h6">About</Typography>
            </Button>
          </NavLink>
          <NavLink to="/projects">
            <Button aria-haspopup="false">
                <Typography variant="h6">Projects</Typography>
            </Button>
          </NavLink>
          <NavLink to="/blog">
            <Button aria-haspopup="false">
                <Typography variant="h6">Blog</Typography>
            </Button>
          </NavLink>
        </Container>
    </footer>
  )
}

export default Footer