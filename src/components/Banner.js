import React, { useState, useEffect } from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Container } from '@material-ui/core'
import { Carousel } from 'react-responsive-carousel';
import Client from '../Client'

const Banner = () => {
  const [banners, setBanners] = useState([])

  const getBanners = async () => {
    try {
      const response = await Client
        .getEntries({
          content_type: 'bannerImage'})
      setBanners(response.items)
    } catch (err) {
      console.error(err)
     }
  }

  useEffect(() => {
    getBanners()
  }, [])

  if (banners.length === 0) {
    return <div>No banners</div>
  }

  const carouselConfig = () => ({
    showArrows: true,
    showStatus: true,
    showIndicators: true,
    infiniteLoop: true,
    showThumbs: false,
    useKeyboardArrows: true,
    autoPlay: true,
    stopOnHover: true,
    swipeable: true,
    dynamicHeight: true,
    emulateTouch: true,
    interval: 5000,
    transitionTime: 200
  })

  return (
    <Carousel {...carouselConfig()} className="carousel">
      {banners.map((banner, idx) => (
        <div className="carousel-image-wrapper" key={idx}>
          <img src={banner.fields.image.fields.file.url} alt="" className="carousel-image" />
          <Container className="carousel-image-text" maxWidth="md">
            <h3 className="banner-title">{ banner.fields.bannerTitle }</h3>
            <p className="banner-byline">{ banner.fields.image.fields.description.toString().length > 80 ? banner.fields.image.fields.description.toString().substring(0, 80) + "... and more!" : banner.fields.image.fields.description }</p>
            </Container>
        </div>
      ))}
    </Carousel>
  )
}

export default Banner
