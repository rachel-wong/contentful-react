import React from 'react'
import { Card, Typography, Button, CardActionArea, CardActions, CardContent, CardMedia } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

  const panelStyles = makeStyles((theme) => ({
    root: {
      height: '100% !important'
    },
    media: {
      height: 140
    }
  }))

const Panel = ({ id, title, text, image}) => {

  return (
    <Card className={ panelStyles.root}>
      <CardActionArea className={ panelStyles.root}>
        <CardMedia component="img" alt={title} height="250" image={image} title={ title } />
        <CardContent className={ panelStyles.root}>
          <Typography gutterBottom variant="h5" color="textSecondary">{title}</Typography>
          <Typography component="p" color="textPrimary">{ text }</Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="secondary">Read more</Button>
      </CardActions>
    </Card>
  )
}

export default Panel
