import React, { useState} from 'react'
import { Paper, Grid, Button, TextField } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search';
import { makeStyles } from '@material-ui/core/styles'

const localStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(4)
  }
}))

const Search = () => {
  const [keyword, setKeyword] = useState("")

  const handleSearch = (e) => {
    e.preventDefault()
  }
  const localClasses = localStyles()

  return (
    <Paper className={ localClasses.container } elevation={3} outlined="true">
      <form noValidate autoComplete="off" onSubmit={ handleSearch }>
        <Grid container spacing={1} alignItems="flex-end">
          <Grid item>
            <SearchIcon />
          </Grid>
          <Grid item>
            <TextField id="standard-basic" label="enter title keywords" onChange={(e) => setKeyword(e.target.value)}/>
          </Grid>
          <Grid item>
            <Button type="submit" size="large" href={ `/search?q=${keyword}`}>Search Site</Button>
          </Grid>
        </Grid>
        </form>
      </Paper>
  )
}

export default Search