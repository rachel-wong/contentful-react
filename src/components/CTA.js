import React from 'react'
import { Grid, ButtonBase, Typography, Paper, Container, Button } from '@material-ui/core'
import SampleImage from '../assets/sample.png'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

const CTA = ({ title, description, slug, image }) => {

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto'
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
    link: {
      display: 'block',
      textDecoration: 'none',
    }
  }));

  const classes = useStyles();

  return (
    <Container maxWidth="md" style={{padding: '2rem'}} >
        <Paper elevation={3} outlined="true" className={ classes.paper}>
        <Grid container spacing={2} className={ classes.root }>
          <Grid item>
            <ButtonBase>
                <img src={image} alt="CTA" className={ classes.image}/>
            </ButtonBase>
          </Grid>
          <Grid item xs={12} md container space={2}>
            <Typography variant="h4">{ title }</Typography>
            <Typography variant="body1" color="textSecondary">{description}</Typography>
            <Button href={ `/projects/${slug}` } variant="contained" color="primary" top={0} right={ 0 } className={ classes.link }>Read more</Button>
          </Grid>
          </Grid>
        </Paper>
      </Container>
  )
}

export default CTA

CTA.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
  url: PropTypes.string.isRequired
}

CTA.defaultProps = {
  title: "CTA Article title",
  description: "CTA article description for a project or a blog post",
  url: "http://www.google.com",
  image: SampleImage
}