import React from 'react'
import { Container, Typography, Button } from '@material-ui/core'
import SampleImage from '../assets/sample.png'

const NotFound = () => {
  return (
    <Container maxWidth="sm" direction="column" justify="center" style={{"textAlign": "center"}}>
      <Typography variant="h3" style={{ "padding": "20px 0 40px"}}>You've gone the wrong way</Typography>
      <img src={SampleImage} style={{ "width": "100%", "display": "block", "maxWidth": "50%", "margin": "0 auto" }} />
      <Button variant="contained" color="secondary" href={"/"} style={{"marginTop": "20px"}}>Go Home</Button>
    </Container>
  )
}

export default NotFound
