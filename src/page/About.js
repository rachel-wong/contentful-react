import React, { useEffect, useState} from 'react'
import { Container, Grid, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Client from '../Client'
import GitHubIcon from '@material-ui/icons/GitHub';
import TwitterIcon from '@material-ui/icons/Twitter';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import FacebookIcon from '@material-ui/icons/Facebook';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';

// local styles needs to be initialised outside of the component function
const useLocalStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(4, 0, 6)
  },
  profileImage: {
    display: "block",
    width: "100%"
  },
  profileHeader: {
    padding: theme.spacing(3),
  },
  profileTagLine: {
    color: theme.palette.text.secondary
  },
  richText: {
    fontSize: '20px',
    lineHeight: '1.8'
  }
}))


const About = () => {

  const localClasses = useLocalStyles() // call localstyles into the component

  const [author, setAuthor] = useState({})

  const getAuthor = async () => {
    try {
      const response = await Client.getEntries({content_type: 'person'})
      setAuthor(response.items.pop().fields)
      let text = documentToHtmlString(author.longBio)
      if (text) {
        document.getElementById('about-rich-text').innerHTML = text
      }
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    getAuthor()
  })

  return (
    <Container className={localClasses.container}>
      <Grid className={localClasses.profileHeader } container spacing={ 3 }>
        <Grid item sm={ 4 }>
          <img
            className={localClasses.profileImage}
            src={author.image && author.image.fields.file.url} alt={author.image && author.image.fields.file.description}
          />
        </Grid>
        <Grid item sm={ 8 }>
          <h1>About {author.name}</h1>
          <Typography variant="body2">{ author.title }</Typography>
        </Grid>
      </Grid>
      <Grid container className={ localClasses.profileTagLine }>
        { author.email &&
          <Grid item>
            <a href={ `mailto:${author.email}` }>
              <Typography variant="body2">
                <AlternateEmailIcon />{ author.email}
              </Typography>
            </a>
          </Grid>
        }
        { author.github &&
          <Grid item>
            <a href={`https://github.com/${author.github}`}>
              <Typography variant="body2">
                <GitHubIcon />{author.github}
              </Typography>
            </a>
          </Grid>
        }
        { author.twitter &&
          <Grid item>
            <a href={`https://www.twitter.com/${author.twitter}`}>
              <Typography variant="body2">
                <TwitterIcon />{ author.twitter}
              </Typography>
            </a>
          </Grid>
        }
        { author.facebook &&
          <Grid item>
            <a href={`https://www.facebook.com/${author.facebook}`}>
              <Typography variant="body2">
                <FacebookIcon />{ author.facebook}
              </Typography>
            </a>
          </Grid>
        }
      </Grid>
      <div id="about-rich-text" className={ localClasses.richText }></div>
    </Container>
  )
}

export default About
