import React, { useEffect, useState } from 'react'
import { Container, Grid, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import useStyles from '../styles'
import Client from '../Client'
import Panel from '../components/Panel'

const localStyles = makeStyles((theme) => ({
  grid: {
    padding: "30px 0 0"
  }
}))

const Blog = () => {
  const localClasses = localStyles()
  const rootClasses = useStyles()

  const [posts, setPosts] = useState([])

  const getPosts = async () => {
    const res = await Client.getEntries({ content_type: 'blogPost'})
    setPosts(res.items)
  }

  useEffect(() => {
    getPosts()
  }, [])

  return (
    <Container className={ rootClasses.container }>
      <Typography variant="h3">My Blog</Typography>
      <Grid container spacing={4} className={ localClasses.grid}>
          {posts && posts.map((item, idx) => (
            <Grid key={idx} item xs={4} >
              <a href={ "/blog/" + item.fields.slug }>
                <Panel id={ item.sys.id } title={ item.fields.title } text={ item.fields.description.length > 180 ? item.fields.description.substring(0, 180) + "..." : item.fields.description } image={ 'https:' + item.fields.heroImage.fields.file.url } />
              </a>
            </Grid>
          ))}
      </Grid>
    </Container>
  )
}

export default Blog
