import React, { useState, useEffect} from 'react'
import { Container, Typography, Button, Box, Grid } from '@material-ui/core'
import useStyles from '../styles'
import { useLocation, useHistory } from 'react-router-dom'
import Client from '../Client'
import BookIcon from '@material-ui/icons/Book';
import DesktopMacIcon from '@material-ui/icons/DesktopMac';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const Results = () => {

  // get search query string
  const { search } = useLocation()
  const keyword = search.split("=").pop().trimStart().trimEnd()

  const [results, setResults] = useState([])

  const classes = useStyles()
  let history = useHistory()

  const goBack = () => {
    history.goBack()
  }

  const getSearchResults = async () => {

    // can only search separately with multiple api call because currently project and blog post do not share a common content model
    // also not searching all fields
    const post_call = Client.getEntries({
      content_type: 'blogPost',
      'fields.title[match]' : keyword
    })

    const project_call = Client.getEntries({
      content_type: 'project',
      'fields.title[match]' : keyword
    })
    try {
      // ineffective and inefficient search - could be improved
      await Promise.allSettled([post_call, project_call])
        .then((res) => {
          const [post_res, project_res] = res
          if (post_res.status === "fulfilled" && project_res.status === "fulfilled") {
            setResults(post_res.value.items.concat(project_res.value.items))
          }
        })
        .catch((err) => console.error(err))
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    getSearchResults()
  })

  return (
    <Container className={ classes.container }>
      <Typography variant="h2">Search Results</Typography>
      <Typography variant="body1" color="textSecondary"></Typography>
      <Container className={classes.container} maxWidth="md">

        {results.length !== 0 && results.map((item, idx) => {
          let contentType = item.sys.contentType.sys.id;

          let url = "";

          if (contentType === "blogPost") {
            url = "/blog"
          } else if (contentType === "project") {
            url = "/projects"
          }

          return (
            <Grid key={ idx } container spacing={ 2 } direction="row" justify="flex-start" alignItems="center" className="row">
              <Grid item xs={2} style={{ "padding": "0 40px" }}>
                {contentType === "blogPost" ? (
                  <Button href={url} variant="outlined" color="primary">
                    <BookIcon />
                  </Button>
                ) : contentType === "project" ? (
                  <Button href={url} variant="outlined" color="secondary">
                    <DesktopMacIcon />
                  </Button>
                ) : ""
                }
              </Grid>
              <Grid item xs={9}>
                <a href={url + `/${item.fields.slug}`} className={ classes.link}>
                  <Typography variant='h5' color='textPrimary'>{ item.fields.title }</Typography>
                </a>
              </Grid>
              <Grid item xs={1}>
                <Button href={url + `/${item.fields.slug}`}>
                  <ArrowForwardIcon />
                </Button>
              </Grid>
            </Grid>
          )
        })}
      </Container>
      <Box>
        <Button variant="contained" color="secondary" onClick={ goBack }>Back</Button>
      </Box>
    </Container>
  )
}

export default Results