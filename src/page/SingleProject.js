import React, { useState, useEffect } from 'react'
import { Container, Typography, Box } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import useStyles from '../styles'
import { makeStyles} from '@material-ui/core/styles'
import Client from '../Client'
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
// import { BLOCKS, MARKS } from "@contentful/rich-text-types";

const localStyles = makeStyles((theme) => ({
  mainImage: {
    width: "100%",
    display: "block",
    objectFit: "cover"
  },
  header: {
    padding: theme.spacing(3)
  },
  imageWrapper: {
    maxHeight: '300px',
    display: 'block',
    overflow: 'hidden',
    paddingTop: "20px",
    paddingBottom: "20px"
  },
  richText: {
    paddingTop: "40px",
    paddingBottom: "20px"
  }
}))

// use slug to build url and find matching post
const SingleProject = () => {
  const { projectslug } = useParams()
  const [project, setProject] = useState({})

  const rootClasses = useStyles()
  const localClasses = localStyles()

  const getProject = async () => {
    try {
      const res = await Client.getEntries({
        content_type: 'project',
        'fields.slug[in]': projectslug.toString()
      })
      setProject(res.items.pop().fields)

    } catch (err) {
      console.error(err)
    }
  }

  // const Bold = ({ children }) => <span className="bold">{children}</span>;

  // const Text = ({ children }) => <span className="align-center">{children}</span>;

  // const options = {
  //   renderMark: {
  //     [MARKS.BOLD]: text => <Bold>{text}</Bold>
  //   },
  //   renderNode: {
  //     [BLOCKS.PARAGRAPH]: (node, children) => {
  //       return <p>{children}</p>;
  //     }
  //   }
  // };

  useEffect(() => {
    getProject()
  })

  return (
    <Container className={rootClasses.container}>
      <header className={ localClasses.header}>
        <Typography variant="h3">{project.title}</Typography>
        <Typography variant="body2" color="textSecondary">Published at: {project.published}</Typography>
      </header>
      <Box className={ localClasses.imageWrapper}>
        <img className={ localClasses.mainImage} src={project.mainImage && "https:" + project.mainImage.fields.file.url} alt={ project.title }/>
      </Box>
      { project.summary &&
        <Box py={ 5 }>
        {documentToReactComponents(project.summary)}
        </Box>}
    </Container>
  )
}

export default SingleProject