import React, { useEffect, useState } from 'react'
import { Container, Grid, Typography } from '@material-ui/core'
import Client from '../Client'
import Panel from '../components/Panel'

const Projects = () => {
  const [projects, setProjects] = useState([])

  const getProjects = async () => {
    try {
      const res = await Client.getEntries({ content_type: 'project' })
      setProjects(res.items)
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    getProjects()
  }, [])

  return (
    <Container>
      <Typography variant="h2">Live projects</Typography>
      <Grid container spacing={ 4 }>
        { projects && projects.map((project, idx) => (
            <Grid key={ idx} item xs={4} >
              <a href={ `/projects/${project.fields.slug}`}>
                <Panel
                  title={project.fields.title}
                  image={project.fields.mainImage.fields.file.url}
                  text={project.fields.title} />
                </a>
              </Grid>
          )
        )}
      </Grid>
    </Container>
  )
}

export default Projects
