import React, { useState, useEffect } from 'react'
import Client from '../Client'
import { CssBaseline, Container, Typography, Grid } from '@material-ui/core'
import Banner from '../components/Banner'
import CTA from '../components/CTA'
import Panel from '../components/Panel'
import useStyles from '../styles'
import Search from '../components/Search'
const Home = () => {
  const classes = useStyles()

  const [latestProject, setLatestProject] = useState({}) // returns one project
  const [posts, setPosts] = useState([])

  const getData = async () => {

    await Promise.allSettled([
      Client.getEntries({
        content_type: 'project', order: '-fields.published,fields.title'
      }),
      Client.getEntries({ content_type: 'blogPost' })
    ]).then((res) => {
      const fulfilled = "fulfilled"
      const [projectData, blogpostData] = res
      if (projectData.status === fulfilled && blogpostData.status === fulfilled) {
        setLatestProject(projectData.value.items.shift().fields)
        setPosts(blogpostData.value.items.slice(0, 3))
      }
    }).catch(err => console.error(err))
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <>
      <Banner />
      <CssBaseline />
      { latestProject &&
        <CTA title={latestProject.title} slug={ latestProject.slug } description={latestProject.blurb} image={latestProject.mainImage && 'https:' + (latestProject.mainImage.fields.file.url).toString()} />
      }
      <Container maxWidth="sm" className={classes.container}>
        <Search />
      </Container>
      <Container className={ classes.container }>
        <Typography gutterBottom variant="h4" color="textPrimary">Latest from the Blog</Typography>
        <Grid container spacing={ 4 }>
          {posts && posts.map((item, idx) => (
            <Grid key={idx} item xs={4}>
              <a href={ `/blog/${item.fields.slug}`}>
                <Panel className={classes.gridItem} title={item.fields.title} text={item.fields.description} image={'https:' + item.fields.heroImage.fields.file.url} style={{ height: '100% !important' }} />
              </a>
            </Grid>
          ))}
        </Grid>
      </Container>
    </>
  )
}

export default Home
