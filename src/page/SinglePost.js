import React, { useEffect, useState } from 'react'
import { Container, Button, Box, Typography } from '@material-ui/core'
import { useParams , useHistory} from 'react-router-dom'
import Client from '../Client'

const marked = require('marked')

// use slug to build url and find matching post
const SinglePost = () => {

  const { blogslug } = useParams()
  let history = useHistory()

  const goHome = () => {
    history.push("/")
  }

  const goBack = () => {
    history.push("/blog")
  }

  const [post, setPost] = useState({})

  const getSinglePost = async () => {
    const res = await Client.getEntries({
      content_type: 'blogPost',
      'fields.slug': blogslug.toString()
    })
    setPost(res.items.pop().fields)
  }

  if (post.body) {
    document.getElementById('post-body').innerHTML = marked(post.body)
  }

  useEffect(() => {
    getSinglePost()
  })

  return (
    <Container>
      <Typography variant="h3">{ post.title} </Typography>
      <Typography variant="h6" color="textSecondary">{new Date(post.publishDate).toDateString()}</Typography>
      {/* {post.description} */}
      <div id="post-body"></div>
      <Box style={{ "padding" : '20px 0 10px'}}>
        <Button size="large" variant="contained" color="secondary" onClick={ goBack }>Go back to Blog</Button>
        <Button size="large" variant="contained" color="primary" onClick={ goHome }>Go Home</Button>
      </Box>
    </Container>
  )
}

export default SinglePost