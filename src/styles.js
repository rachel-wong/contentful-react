import { makeStyles } from '@material-ui/core/styles';

// CSS in JS
const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '100vh'
  },
  container: {
    padding: theme.spacing(4, 0, 6)
  },
  mainlogo: {
    padding: theme.spacing(2, 2)
  },
  gridItem: {
    height: '100%'
  },
  richText: {
    fontSize: '20px',
    lineHeight: '1.8'
  },
  link: {
    textDecoration: 'none',
    color: 'inherit',
  }
}))

export default useStyles