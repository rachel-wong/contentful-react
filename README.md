## :books: Contentful with React

> Testing out another headless CMS with react, learning more about Material UI components and CSS-in-JS

![screenshot of Contentful blog](/src/assets/contenful_demo.gif)

**Deployed to https://distracted-sinoussi-7f240d.netlify.app/**

#### Key Dependencies

- @contentful/rich-text-react-renderer
- @contentful/rich-text-types
- contentful
- dotenv (to hide Space ID and client key)
- @material-ui/core
- @material-ui/icons
- react-material-ui-carousel

### Learning notes

Documenting some errors, things not working as expected for posterity. Errors will range from basic to issues that range outside of my current existing knowledge base.

* Encountered an issue with the routes being set up with `react-router-dom` where any `NavLink` element will inadvertently point to home page. It wasn't an issue of the router not being able to distinguish between the home (root) route and other routes. Rather, I had a typo with the `Route` not using the correct attribute (`path` not `to`).

```javascript
  // App.js
  <Route path="/" exact component={ Home }>

  // Navbar.js
  <NavLink to="/">Go to home</NavLink>
```

* Apparently MaterialUI does not play well with React v17 as of writing. I couldn't get around the dependency issue when I tried to install the material ui carousel. I had tried uninstalling `@material-ui/core`, removing `node_modules` and reinstalling everything, changing the version of react to a lower one (v.16.3.1). I eventually resorted to using a separate npm package altogether `react-responsive-carousel`. I'm not sure how to get around the issue even with [research](https://stackoverflow.com/questions/64625021/when-installing-material-ui-i-am-prompted-with-a-series-of-errors/64626055) or whether installing yet another package is actually the best way to go.

* To use the slug in the route, it is best that the field is set as "UNIQUE" in the Contentful content modelling settings.

* Not a good idea to learn a component framework as large as Material UI while trying to figure out the ins-and-outs of querying the headless CMS api. Best to separate between the two.

* The search function is not an inefficient or elegant solution. It can only search when the button is pressed and it only searches within the "title" field of Blog Post and Project content types. It is not a "global site search". While the search is functioning within these parameters, two key things can be learnt from this:

1. the implementation of a search function would require passing data props between sibling components. Therefore, ContextAPI should be used instead of relying solely on `useEffect` hook on every page.

2. The Contentful Content Delivery API seems to only allow searching querying one content type at a time. In this case where the site makes a distinction between blog post and project post, a parent content model ("post"?) with commmon fields should be used instead.

* Netlify-Deploy process requires deploying the react build (and adding the environment variables. Then there is a separate Netlify app attached to contentful to be initiated. The contentful side of the deployment is largely automated, and only requires you to specify the netlify link of the deployed react app.

* Made rookie error by breaking apart the props when looping through an array of objects. It is best to pass through the entire object and manage the error handling at the child component level for better reusability (for example: not every type of content using the same 'Panel' might have descriptive text)

* Not entirely sure why but deployment needed several manual retries. Remember to add a webhook to make sure that the data created in the contentful CMS will be automatically built over to the deployed site.